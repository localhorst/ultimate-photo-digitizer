#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" Author:                     Hendrik Schutter, mail@hendrikschutter.com
"""

import cv2
import sys
import os
import subprocess

def convert_to_avif(input_file_path, output_dir_path):
    # print("input_file_path: " + str(input_file_path))
    # print("output_dir_path: " + str(output_dir_path))

    output_file_path = os.path.join(output_dir_path,(os.path.splitext(os.path.basename(os.path.normpath(input_file_path)))[0] + ".avif"))

    cmd_handle = subprocess.Popen(["avifenc", "--jobs", "all", input_file_path, output_file_path], stdout=subprocess.PIPE)
    cmd_handle.communicate()[0]
    if cmd_handle.returncode != 0:
        raise Exception("Unable to convert via avifenc! Return code: " + str(cmd_handle.returncode))
    
    cmd_handle = subprocess.Popen(["sync",],stdout=subprocess.PIPE,)
    cmd_handle.communicate()[0]
    if cmd_handle.returncode != 0:
        raise Exception("Unable to sync! Return code: " + str(cmd_handle.returncode))

def main():
    print("starting ...")

    destination_dir_path = -1
    source_dir_path = -1

    for argument in sys.argv:
        if argument.startswith("destination_dir"):
            destination_dir_path = argument.split("=")[1]
        if argument.startswith("source_dir"):
            source_dir_path = argument.split("=")[1]

    if (destination_dir_path == -1) or (source_dir_path == -1):
        print("Unable to parse config!")
        print("Example usage:")
        print(
            "     python convert_photos.py source_dir=tmp/ destination_dir=photos/"
        )
        sys.exit(-1)

    failed_conventions = 0

    for path in os.listdir(source_dir_path):
        if os.path.isfile(os.path.join(source_dir_path, path)):
            print("Found photo: " + str(os.path.join(source_dir_path, path)), end=" ")
            sys.stdout.flush()
            try:
                convert_to_avif(os.path.join(source_dir_path, path), destination_dir_path)
                print("  →   Successfully converted photo to avif")
            except Exception as e:
                print(str(e))
                failed_conventions = failed_conventions + 1

    if(failed_conventions == 0):
        print("\nConvert without error")
        sys.exit(0)
    else:
        print("Unable to convert " + str(failed_conventions) + " photos.")
        print("\nExiting with error")
        sys.exit(-1)

if __name__ == "__main__":
    main()
