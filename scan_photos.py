#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" Author:                     Hendrik Schutter, mail@hendrikschutter.com
"""

import os
import errno
import time
import shutil
import sys
import subprocess

scan_cmd = "scanimage --progress --format=png --mode Color --resolution 2400 --verbose --depth 16 --output-file "

def main():
    print("starting ...")

    destination_dir_path = -1

    for argument in sys.argv:
        if argument.startswith('destination_dir'):
            destination_dir_path = argument.split("=")[1]
        
    if (destination_dir_path == -1):
        print("Unable to parse config!")
        print("Example usage:")
        print("     python scan_photos.py destination_dir=scans/")
        sys.exit(-1)

    filename = time.strftime("%Y-%m-%d_%H-%M-%S.png")
    #print(filename)

    filepath = os.path.join(destination_dir_path, filename)
    #print(filepath)

    cmd = scan_cmd + filepath
    
    print(cmd)
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
    (output, err) = p.communicate()
    
    p_status = p.wait()
    #print("Command output : ",output.decode())
    print("Command exit status/return code : ", p_status)

    if(int(p_status) == 0):
        print("Scan successful")
    else:
        print("Scan failed!")
        if os.path.exists(filepath):
            os.remove(filepath)

if __name__ == "__main__":
        main()