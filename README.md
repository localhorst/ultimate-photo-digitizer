# ultimate-photo-digitizer

## Setup

```
pip install opencv-python-headless

zypper install avif-tools

```
## Usage

### Step 1: Scan analog photos

1. Create `scans` folder.
2. Place four analog photos in scanner bed, align edges with scanner. But keep a margin of 1cm between photos and between the scanner.
3. Scan the photos via `python3 ./scan_photos.py destination_dir=scans/`
4. Repeat step 2 to 3 until all photos are scanned.

### Step 2: Extract the single photos from the scan

1. Create `tmp` folder.
2. Extract the photos via `python3 extract_photos.py source_dir=scans/ destination_dir=tmp/`

### Step 3: Rotate the extracted photos

1. Use any image viewer, like gwenview, to rotate and save the photos if necessary.

### Step 4: Convert the extracted photos to AVIF

1. Create `photos` folder.
2. Convert the photos via `python3 convert_photos.py source_dir=tmp/ destination_dir=photos/`

## Sync folders between machines

### Push scans to server
`rsync -Pav -e "ssh -i $HOME/.ssh/SDS" /home/hendrik/git/ultimate-photo-digitizer/scans/ hendrik@10.0.0.25:/home/hendrik/git/ultimate-photo-digitizer/scans/`

### Pull tmp from server
`rsync -Pav -e "ssh -i $HOME/.ssh/SDS" hendrik@10.0.0.25:/home/hendrik/git/ultimate-photo-digitizer/tmp/ /home/hendrik/git/ultimate-photo-digitizer/tmp/`

### Push tmp to server
`rsync -Pav -e "ssh -i $HOME/.ssh/SDS" /home/hendrik/git/ultimate-photo-digitizer/tmp/ hendrik@10.0.0.25:/home/hendrik/git/ultimate-photo-digitizer/tmp/`

### Pull photos from server
`rsync -Pav -e "ssh -i $HOME/.ssh/SDS" hendrik@10.0.0.25:/home/hendrik/git/ultimate-photo-digitizer/photos/ /home/hendrik/git/ultimate-photo-digitizer/photos/`
